import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

public class GUI {
	private String password = "U2";
	private boolean access = false;
	private Database DB;
	private Customer customer;
	private SubScription subs;
	private Delivery deliv;
	//private Bills bills;

	private CardLayout cards;
	private JPanel mid;

	JTextField txtCustomerID;
	JTextField txtSubscriptionID;
	ArrayList newsP;

	JComboBox customersFound;

	JLabel firstName;
	JLabel lastName;
	JLabel dateOfBirth;
	JLabel addressStreet;
	JLabel phone;
	JLabel email;
	JLabel areaCode;
	JLabel custId;
	JLabel subsId;
	JPanel b3;

	JTextField fNtxt;
	JTextField lNtxt;
	JTextField dOBtxt;
	JTextField aStxt;
	JTextField ptxt;
	JTextField etxt;
	JTextField aCtxt;
	JTextField cIdtxt;
	JTextField sIdtxt;
	JPanel b4;

	private int choiceFlag;
	private int searchFlag = 0;
	private JComboBox<String> txtAreaCode;
	private JComboBox<String> deliveriesDetails;
	private JTextField txtDelPName;
	private JTextField txtDelPID;
	private JTextField custNameDel;
	private JTextField txtAddress;
	private JTextField txtPhNo;
	private JTextField subsListDel;
	private JTextField txtDelAreaDetails;
	private JTextField txtDelPNameDetails;
	private JTextField txtDelPIDDetails;
	private JTextField textArea;
	private JTextField textSubsID;
	private JTextField txtDateDel;
	private JTextArea logs;
	private JTextField textFieldFirstName;
	private JTextField textFieldPhoneNo;
	private JTextField textFieldAddress;
	private JTextField textField;
	private JComboBox comboBoxSubsID = new JComboBox();

	public GUI() {
		JFrame frame = new JFrame();
		frame.setExtendedState(frame.MAXIMIZED_BOTH);
		Font f1 = new Font("Times New Roman", Font.PLAIN, 20);
		frame.setTitle("");

		Container cp = frame.getContentPane();

		Font f = new Font("ALGERIAN", Font.ITALIC | Font.BOLD, 18);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createTitledBorder("  NEWS Agency System   "));
		JPanel top = new JPanel();
		top.setPreferredSize(new Dimension(800, 300));
		top.setBackground(new Color(95, 158, 160));
		top.setLayout(new BorderLayout());
		top.setBorder(new EmptyBorder(15, 30, 10, 30));
		JPanel left = new JPanel();
		left.setBackground(new Color(95, 158, 160));
		left.setBorder(new EmptyBorder(0, 10, 0, 10));
		mid = new JPanel();
		JPanel right = new JPanel();
		right.setBackground(new Color(95, 158, 160));
		right.setBorder(new EmptyBorder(0, 10, 0, 10));
		JPanel bot = new JPanel();
		bot.setPreferredSize(new Dimension(800, 70));
		bot.setLayout(new GridLayout(1, 1));
		bot.setBackground(new Color(95, 158, 160));

		logs = new JTextArea(20, 20);
		JScrollPane scrollLogs = new JScrollPane(logs);
		top.add(scrollLogs, BorderLayout.CENTER);
		logs.setFont(f);

		cards = new CardLayout();
		mid.setLayout(cards);

		JPanel login = new JPanel();
		JPanel custAcc = new JPanel();
		JPanel deliveries = new JPanel();
		JPanel subscriptions = new JPanel();
		JPanel deliveryDetails = new JPanel();
		JPanel bills = new JPanel();
		bills.setBackground(new Color(153, 204, 204));
		deliveryDetails.setBackground(new Color(169, 169, 169));

		mid.add(login, "login");
		mid.add(custAcc, "customer");
		mid.add(deliveries, "deliveries");
		mid.add(subscriptions, "subscriptions");
		mid.add(deliveryDetails, "deliveryDetails");
		mid.add(bills,"bills");
		JButton gotoCustomer = new JButton("to Customer");
		JButton gotSubscription = new JButton("to Subscritpion");
		JButton gotoDelivery = new JButton("to Delivery");
		JButton gotoBills = new JButton("to Bills");
		JButton gotoHolidays = new JButton("Log Out");
		JPanel botBut = new JPanel();
		botBut.setLayout(new GridLayout(1, 5));
		botBut.setBackground(new Color(95, 158, 160));
		botBut.setBorder(BorderFactory.createEmptyBorder(10, 30, 15, 30));
		botBut.add(gotoCustomer);
		botBut.add(gotSubscription);
		botBut.add(gotoDelivery);
		botBut.add(gotoBills);
		botBut.add(gotoHolidays);
		bot.add(botBut);
		botBut.setVisible(false);

		ActionListener switchingCards = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == gotoCustomer) {
					cards.show(mid, "customer");
				}
				if (e.getSource() == gotSubscription) {
					cards.show(mid, "subscriptions");
				}
				if (e.getSource() == gotoDelivery) {
					cards.show(mid, "deliveries");
				}
				if (e.getSource() == gotoBills) {

				}
				if (e.getSource() == gotoHolidays) {
					System.exit(0);

				}
				if (e.getSource() == gotoBills) {
					cards.show(mid, "bills");
				}
			}
		};
		gotoCustomer.addActionListener(switchingCards);
		gotSubscription.addActionListener(switchingCards);
		gotoDelivery.addActionListener(switchingCards);
		gotoBills.addActionListener(switchingCards);
		gotoHolidays.addActionListener(switchingCards);

		/************************************************************************************************************************************/
		/*********************************************
		 * LOGIN CARD
		 **************************************************************************/
		login.setLayout(new GridLayout(5, 3));
		login.setBorder(BorderFactory.createTitledBorder("  LOGIN   "));
		login.setBackground(new Color(176, 196, 222));
		JButton manager = new JButton("System Access");
		manager.setFont(f);
		manager.setForeground(Color.RED);
		JButton delivery = new JButton("Deliveries");
		delivery.setFont(f);
		final Color dark_green = new Color(0, 153, 0);
		delivery.setForeground(dark_green);
		for (int i = 0; i < 15; i++) {
			if (i == 4) {
				login.add(manager);
			} else if (i == 10) {
				login.add(delivery);
			} else {
				login.add(Box.createHorizontalGlue());
			}
		}

		/************************************************************************************************************************************/
		/*********************************************
		 * CUSTOMER CARD
		 ***********************************************************************/
		custAcc.setBackground(Color.orange);
		custAcc.setBorder(BorderFactory.createTitledBorder("  CUSTOMER   "));
		JRadioButton cre = new JRadioButton("  Create new Account  ");
		cre.setOpaque(false);
		cre.setSelected(true);
		JRadioButton arch = new JRadioButton("  Archive Account  ");
		arch.setOpaque(false);
		JRadioButton sea = new JRadioButton("  Search Account  ");
		sea.setOpaque(false);
		JButton archB = new JButton("Archive Account");
		Box b1 = new Box(BoxLayout.Y_AXIS);
		JPanel b1P = new JPanel();
		b1P.setBackground(Color.red);
		b1P.setBorder(BorderFactory.createTitledBorder("  Options   "));
		b1P.add(b1);
		b1.add(cre);
		b1.add(sea);
		b1.add(arch);
		b1.add(archB);
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(cre);
		bg1.add(arch);
		bg1.add(sea);

		firstName = new JLabel("First Name:");
		lastName = new JLabel("Last Name:");
		dateOfBirth = new JLabel("Year of birth:  ");
		addressStreet = new JLabel("Address:");
		phone = new JLabel("Phone No.:");
		email = new JLabel("E-mail:");
		areaCode = new JLabel("Area Code:");
		custId = new JLabel("Customer ID:  ");
		subsId = new JLabel("Subscrip. ID:  ");
		b3 = new JPanel();
		b3.setBackground(Color.orange);
		b3.setLayout(new GridLayout(8, 1, 0, 5));
		b3.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));
		b3.add(firstName);
		b3.add(lastName);
		b3.add(dateOfBirth);
		b3.add(addressStreet);
		b3.add(phone);
		b3.add(email);
		b3.add(areaCode);

		fNtxt = new JTextField(15);
		lNtxt = new JTextField(15);
		dOBtxt = new JTextField(15);
		aStxt = new JTextField(10);
		ptxt = new JTextField("0000000000", 15);
		etxt = new JTextField(15);
		aCtxt = new JTextField(15);
		cIdtxt = new JTextField(15);
		sIdtxt = new JTextField(15);
		b4 = new JPanel();
		b4.setBackground(Color.orange);
		b4.setLayout(new GridLayout(8, 1, 0, 5));
		b4.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 80));
		b4.add(fNtxt);
		b4.add(lNtxt);
		b4.add(dOBtxt);
		b4.add(aStxt);
		b4.add(ptxt);
		b4.add(etxt);
		b4.add(aCtxt);

		Box b5 = new Box(BoxLayout.X_AXIS);
		b5.add(b1P);
		b5.add(b3);
		b5.add(b4);

		JButton creB = new JButton("Create Account");
		JButton modB = new JButton("Modify Account");
		JPanel b6 = new JPanel();
		b6.setBackground(Color.orange);
		b6.setLayout(new GridLayout(2, 1, 50, 0));
		b6.add(modB);
		b6.add(creB);

		JCheckBox nm = new JCheckBox("Name");
		nm.setOpaque(false);
		JCheckBox cusId = new JCheckBox("Cust. ID");
		cusId.setOpaque(false);
		JCheckBox add = new JCheckBox("Address");
		add.setOpaque(false);
		JCheckBox subId = new JCheckBox("Subs. ID");
		subId.setOpaque(false);
		JButton searchB = new JButton("Search Account");
		Box b2 = new Box(BoxLayout.Y_AXIS);
		b2.add(nm);
		b2.add(add);
		b2.add(cusId);
		b2.add(subId);
		b2.add(searchB);
		JPanel b2P = new JPanel();
		b2P.setBackground(Color.red);
		b2P.setBorder(BorderFactory.createTitledBorder(" Search by:  "));
		b2P.add(b2);
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(nm);
		bg2.add(add);
		bg2.add(cusId);
		bg2.add(subId);

		customersFound = new JComboBox();
		customersFound.setModel(
				new DefaultComboBoxModel<String>(new String[] { " List of Customers found in the Data Base " }));
		customersFound.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (customersFound.getSelectedIndex() > 0) {
					String currentCus = customersFound.getSelectedItem().toString();
					String[] values = currentCus.split(",");
					cIdtxt.setText(values[0]);
					fNtxt.setText(values[1]);
					lNtxt.setText(values[2]);
					dOBtxt.setText(values[3]);
					aStxt.setText(values[4]);
					ptxt.setText(values[5]);
					etxt.setText(values[6]);
					aCtxt.setText(values[7]);
					sIdtxt.setText(values[8]);

				}
			}
		});

		Box b7 = new Box(BoxLayout.Y_AXIS);
		b7.add(b6);
		b7.add(b2P);

		Box b8 = new Box(BoxLayout.X_AXIS);
		b8.add(b5);
		b8.add(b7);
		Box b9 = new Box(BoxLayout.Y_AXIS);
		b9.add(customersFound);
		b9.add(b8);

		custAcc.add(b9);

		/************************************************************************************************************************************/
		/*********************************************
		 * DELIVERY CARD
		 ************************************************************************/
		deliveries.setBackground(new Color(176, 224, 230));
		deliveries.setBorder(BorderFactory.createTitledBorder(" DELIVERIES "));
		deliveries.setLayout(null);

		JLabel lblAreaCode = new JLabel("Area Code");
		JLabel lblDelPName = new JLabel("Delivery Person Name");
		JLabel lblDelPID = new JLabel("Delivery Person ID");
		JLabel lblDeliveryDetails = new JLabel("Delivery Details:");
		deliveriesDetails = new JComboBox();
		deliveriesDetails.setBounds(22, 64, 739, 22);
		deliveries.add(deliveriesDetails);
		deliveriesDetails.setModel(
				new DefaultComboBoxModel<String>(new String[] { " List of Deliveries for the current area " }));
		deliveriesDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (deliveriesDetails.getSelectedIndex() > 0) {
					String currentCus = deliveriesDetails.getSelectedItem().toString();
					String[] values = currentCus.split(" @ ");
					textSubsID.setText(values[0]);
					custNameDel.setText(values[1]);
					txtAddress.setText(values[2]);
					txtPhNo.setText(values[3]);
					subsListDel.setText(values[4]);
					txtDateDel.setText(values[5]);
				}
			}
		});

		String[] ar = { "Choose Area", "1", "2", "3", "4" };
		txtAreaCode = new JComboBox();
		txtAreaCode.setModel(
				new DefaultComboBoxModel<String>(new String[] { "Choose Area", "1", "2", "3", "4" }));
		txtDelPName = new JTextField();
		txtDelPID = new JTextField();
		deliveries.add(lblAreaCode);
		deliveries.add(txtAreaCode);
		deliveries.add(lblDelPName);
		deliveries.add(txtDelPName);
		deliveries.add(lblDelPID);
		deliveries.add(txtDelPID);
		deliveries.add(lblDeliveryDetails);
		JLabel lblCustomerName = new JLabel("Customer Name");

		deliveries.add(lblCustomerName);

		txtAreaCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (txtAreaCode.getSelectedIndex() > 0) {
					String choice = (String) txtAreaCode.getSelectedItem();
					int areaId = Integer.parseInt(choice);
					System.out.println(areaId);
					deliv.chooseArea(areaId);
					deliveriesDetails.setModel(
							new DefaultComboBoxModel<String>(new String[] { " List of Deliveries for the current area " }));
					String area = ""+areaId;
					try {
						DB.DeliveryHistory(area);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		lblAreaCode.setBounds(24, 30, 69, 24);
		txtAreaCode.setBounds(90, 32, 106, 20);
		// txtAreaCode.setColumns(10);
		lblDelPName.setBounds(210, 30, 129, 24);
		txtDelPName.setColumns(10);
		txtDelPName.setBounds(340, 32, 198, 20);
		lblDelPID.setBounds(550, 30, 106, 24);
		txtDelPID.setColumns(10);
		txtDelPID.setBounds(655, 32, 106, 20);
		lblDeliveryDetails.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblDeliveryDetails.setForeground(Color.BLUE);
		lblDeliveryDetails.setBounds(20, 90, 211, 24);

		lblCustomerName.setBounds(24, 148, 99, 14);
		custNameDel = new JTextField();
		custNameDel.setBounds(133, 145, 114, 20);
		deliveries.add(custNameDel);
		custNameDel.setColumns(10);

		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(24, 176, 99, 14);
		deliveries.add(lblAddress);

		txtAddress = new JTextField();
		txtAddress.setColumns(10);
		txtAddress.setBounds(133, 173, 114, 20);
		deliveries.add(txtAddress);

		JLabel lblPhNo = new JLabel("Phone No");
		lblPhNo.setBounds(24, 203, 99, 14);
		deliveries.add(lblPhNo);

		txtPhNo = new JTextField();
		txtPhNo.setColumns(10);
		txtPhNo.setBounds(133, 200, 114, 20);
		deliveries.add(txtPhNo);

		JLabel lblSubscriptions = new JLabel("Subscriptions");
		lblSubscriptions.setBounds(24, 230, 99, 14);
		deliveries.add(lblSubscriptions);

		subsListDel = new JTextField();
		subsListDel.setColumns(10);
		subsListDel.setBounds(133, 227, 114, 20);
		deliveries.add(subsListDel);

		JLabel lblDelivered = new JLabel("Delivered ?");
		lblDelivered.setBounds(24, 257, 88, 14);
		deliveries.add(lblDelivered);

		JCheckBox chckbxNo = new JCheckBox("No");
		chckbxNo.setBounds(133, 256, 46, 23);
		deliveries.add(chckbxNo);

		textSubsID = new JTextField();
		textSubsID.setColumns(10);
		textSubsID.setBounds(133, 116, 114, 20);
		deliveries.add(textSubsID);

		JLabel labelSubsID = new JLabel("Subscription ID");
		labelSubsID.setBounds(24, 121, 99, 14);
		deliveries.add(labelSubsID);

		JButton deliveryModify = new JButton("Modify ");
		deliveryModify.setBounds(282, 198, 97, 25);
		deliveries.add(deliveryModify);

		JButton submitUndelivered = new JButton("Submit");
		submitUndelivered.setBounds(282, 246, 97, 25);
		deliveries.add(submitUndelivered);

		JButton printDelList = new JButton("PRINT");
		printDelList.setBounds(600, 159, 97, 48);
		deliveries.add(printDelList);

		JButton btnSaveUndelivered = new JButton("Save Undelivered");
		btnSaveUndelivered.setBounds(546, 225, 151, 48);
		deliveries.add(btnSaveUndelivered);

		txtDateDel = new JTextField();
		txtDateDel.setColumns(10);
		txtDateDel.setBounds(295, 116, 114, 20);
		deliveries.add(txtDateDel);

		JLabel lblDateDel = new JLabel("Date");
		lblDateDel.setBounds(259, 119, 99, 14);
		deliveries.add(lblDateDel);

		ActionListener DeliveryButtonsEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object target = e.getSource();
				if (target == deliveryModify) {

				}
				if (target == submitUndelivered) {
					int areaCode = txtAreaCode.getSelectedIndex();
					String date = txtDateDel.getText();
					String subID = textSubsID.getText();
					String SubList = subsListDel.getText();
					String name = custNameDel.getText();
					String address = txtAddress.getText();
					String phone = txtPhNo.getText();
					try {
						deliv.Undelivery(subID,date,areaCode,SubList,name,address,phone);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (target == printDelList) {
					String areaCode = (String) txtAreaCode.getSelectedItem();
					logs.append("Deliveries for Area "+areaCode+"\n");
					try {
						DB.PrintDelivery(areaCode);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (target == btnSaveUndelivered) {
					String areaCode = txtAreaCode.getSelectedIndex()+"";
			
					DB.unFile(areaCode);
				}
			}
		};
		printDelList.addActionListener(DeliveryButtonsEvent);
		btnSaveUndelivered.addActionListener(DeliveryButtonsEvent);
		submitUndelivered.addActionListener(DeliveryButtonsEvent);
		// cards.show(mid, "customer");

		/************************************************************************************************************************************/
		/*********************************************
		 * DELIVERY DETAILS CARD
		 ************************************************************************/
		// This card is from the Manager Login(For manager to assign the
		// delivery area for the delivery person)

		deliveryDetails.setBorder(BorderFactory.createTitledBorder(" DELIVERY DETAILS "));
		deliveryDetails.setLayout(null);

		JLabel lblDelAreaDetails = new JLabel("Area Code");
		JLabel lblDelPNameDetails = new JLabel("Delivery Person Name");
		JLabel lblDelPIDDetails = new JLabel("Delivery Person ID");
		JLabel lblArea = new JLabel("Delivery Area");

		txtDelAreaDetails = new JTextField();
		txtDelPNameDetails = new JTextField();
		txtDelPIDDetails = new JTextField();
		textArea = new JTextField();

		lblDelAreaDetails.setBounds(37, 54, 69, 24);
		txtDelAreaDetails.setColumns(10);
		lblDelPNameDetails.setBounds(37, 89, 150, 24);
		txtDelPNameDetails.setColumns(10);
		txtDelPNameDetails.setBounds(170, 91, 129, 20);
		lblDelPIDDetails.setBounds(37, 124, 106, 24);
		txtDelPIDDetails.setColumns(10);
		txtDelPIDDetails.setBounds(170, 124, 129, 20);
		lblArea.setBounds(37, 159, 106, 24);
		textArea.setColumns(10);
		textArea.setBounds(170, 159, 129, 20);

		deliveryDetails.add(lblDelAreaDetails);
		deliveryDetails.add(lblDelPNameDetails);
		deliveryDetails.add(lblDelPIDDetails);
		deliveryDetails.add(lblArea);

		deliveryDetails.add(txtDelAreaDetails);
		deliveryDetails.add(txtDelPNameDetails);
		deliveryDetails.add(txtDelPIDDetails);
		deliveryDetails.add(textArea);

		/************************************************************************************************************************************/
		/*********************************************
		 * SUBSCRIPTION CARD
		 ********************************************************************/

		subscriptions.setBorder(BorderFactory.createTitledBorder("  Subscription Details   "));
		subscriptions.setBackground(new Color(100, 149, 237));
		JPanel subsPanelL = new JPanel();
		JPanel subsPanelR = new JPanel();
		JPanel subsPanelTOP = new JPanel();
		JPanel subsPanelD = new JPanel();
		JPanel subsPanelW = new JPanel();
		JPanel subsPanelM = new JPanel();
		subsPanelTOP.setBorder(BorderFactory.createTitledBorder("Type Of Publication"));
		subscriptions.add(subsPanelTOP);
		subsPanelTOP.add(subsPanelD);
		subsPanelTOP.add(subsPanelW);
		subsPanelTOP.add(subsPanelM);
		subsPanelTOP.setBackground(new Color(100, 149, 237));
		subsPanelL.setBackground(new Color(100, 149, 237));
		subsPanelL.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelL.setBorder(BorderFactory.createEmptyBorder(20, 40, 0, 0));
		subscriptions.add(subsPanelL);
		subsPanelR.setBackground(new Color(100, 149, 237));
		subsPanelR.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelR.setBorder(BorderFactory.createEmptyBorder(20, 40, 0, 0));
		subscriptions.add(subsPanelR);
		subsPanelD.setBackground(new Color(100, 149, 237));
		subsPanelD.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelD.setBorder(BorderFactory.createEmptyBorder(20, 40, 0, 0));
		subsPanelW.setBackground(new Color(100, 149, 237));
		subsPanelW.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelW.setBorder(BorderFactory.createEmptyBorder(20, 40, 0, 0));
		subsPanelM.setBackground(new Color(100, 149, 237));
		subsPanelM.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelM.setBorder(BorderFactory.createEmptyBorder(20, 40, 0, 0));

		JLabel lblDaily = new JLabel("Daily");
		lblDaily.setForeground(new Color(255, 0, 0));
		subsPanelD.add(lblDaily);
		JRadioButton rdbtnNewsPaper1 = new JRadioButton("Newspaper1");
		subsPanelD.add(rdbtnNewsPaper1);
		rdbtnNewsPaper1.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnNewsPaper2 = new JRadioButton("Newspaper2");
		subsPanelD.add(rdbtnNewsPaper2);
		rdbtnNewsPaper2.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnNewsPaper3 = new JRadioButton("Newspaper3");
		subsPanelD.add(rdbtnNewsPaper3);
		rdbtnNewsPaper3.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnNewsPaper4 = new JRadioButton("Newspaper4");
		subsPanelD.add(rdbtnNewsPaper4);
		rdbtnNewsPaper4.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnNewsPaper5 = new JRadioButton("Newspaper5");
		subsPanelD.add(rdbtnNewsPaper5);
		rdbtnNewsPaper5.setBackground(new Color(100, 149, 237));

		JLabel lblWeekly = new JLabel("Weekly");
		lblWeekly.setForeground(new Color(255, 0, 0));
		subsPanelW.add(lblWeekly);
		JRadioButton rdbtnMagazine1 = new JRadioButton("Magazine1");
		subsPanelW.add(rdbtnMagazine1);
		rdbtnMagazine1.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnMagazine2 = new JRadioButton("Magazine2");
		subsPanelW.add(rdbtnMagazine2);
		rdbtnMagazine2.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnMagazine3 = new JRadioButton("Magazine3");
		subsPanelW.add(rdbtnMagazine3);
		rdbtnMagazine3.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnMagazine4 = new JRadioButton("Magazine4");
		subsPanelW.add(rdbtnMagazine4);
		rdbtnMagazine4.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnMagazine5 = new JRadioButton("Magazine5");
		subsPanelW.add(rdbtnMagazine5);
		rdbtnMagazine5.setBackground(new Color(100, 149, 237));
		// Test

		JLabel lblMonthly = new JLabel("Monthly");
		lblMonthly.setForeground(new Color(255, 0, 0));
		subsPanelM.add(lblMonthly);
		JRadioButton rdbtnMagazineM1 = new JRadioButton("Magazine M1");
		subsPanelM.add(rdbtnMagazineM1);
		rdbtnMagazineM1.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnMagazineM2 = new JRadioButton("Magazine M2");
		subsPanelM.add(rdbtnMagazineM2);
		rdbtnMagazineM2.setBackground(new Color(100, 149, 237));
		JRadioButton rdbtnMagazineM3 = new JRadioButton("Magazine M3");
		subsPanelM.add(rdbtnMagazineM3);
		rdbtnMagazineM3.setBackground(new Color(100, 149, 237));

		JLabel lblCustomerId = new JLabel("Customer ID");
		subsPanelL.add(lblCustomerId);
		txtCustomerID = new JTextField();
		subsPanelL.add(txtCustomerID);
		JLabel lblSubscriptionId = new JLabel("Subscription ID");
		subsPanelL.add(lblSubscriptionId);
		txtSubscriptionID = new JTextField();
		subsPanelL.add(txtSubscriptionID);
		JLabel lblTotal = new JLabel("Total: � ");
		subsPanelL.add(lblTotal);
		JTextField textTotal = new JTextField();
		subsPanelL.add(textTotal);
		JPanel subsBut = new JPanel();
		subsBut.setBackground(new Color(100, 149, 237));
		subsBut.setLayout(new GridLayout(8, 1, 0, 5));
		JButton btnSubscribe = new JButton("Subscribe");
		subsBut.add(btnSubscribe);
		subsBut.add(Box.createHorizontalGlue());
		JButton btnRenew = new JButton("Renew");
		subsBut.add(btnRenew);
		subsBut.add(Box.createHorizontalGlue());
		JButton btnModify = new JButton("Modify");
		subsBut.add(btnModify);
		subsBut.add(Box.createHorizontalGlue());
		JButton btnSearch = new JButton("Search");
		subsBut.add(btnSearch);
		subsBut.add(Box.createHorizontalGlue());
		subscriptions.add(subsBut);
		newsP = new ArrayList<>();
		newsP.add(rdbtnNewsPaper1);
		newsP.add(rdbtnNewsPaper2);
		newsP.add(rdbtnNewsPaper3);
		newsP.add(rdbtnNewsPaper4);
		newsP.add(rdbtnNewsPaper5);
		newsP.add(rdbtnMagazine1);
		newsP.add(rdbtnMagazine2);
		newsP.add(rdbtnMagazine3);
		newsP.add(rdbtnMagazine4);
		newsP.add(rdbtnMagazine5);
		newsP.add(rdbtnMagazineM1);
		newsP.add(rdbtnMagazineM2);
		newsP.add(rdbtnMagazineM3);

		modB.setVisible(false);
		archB.setVisible(false);
		customersFound.setVisible(false);
		b2P.setVisible(false);
		
/******************************************************************************************************************************************
 										BILLS CARD
 
******************************************************************************************************************************************/

		bills.setBorder(BorderFactory.createTitledBorder("  BILLS   "));
		bills.setLayout(null);
		
		JLabel lblFirstName = new JLabel("Customer Name");
		lblFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblFirstName.setBounds(246, 113, 140, 21);
		bills.add(lblFirstName);
		
		textFieldFirstName = new JTextField();
		textFieldFirstName.setBounds(398, 116, 135, 21);
		bills.add(textFieldFirstName);
		textFieldFirstName.setColumns(10);
		
		JLabel lblPhoneNo = new JLabel("Phone No");
		lblPhoneNo.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblPhoneNo.setBounds(246, 150, 81, 21);
		bills.add(lblPhoneNo);
		
		textFieldPhoneNo = new JTextField();
		textFieldPhoneNo.setColumns(10);
		textFieldPhoneNo.setBounds(398, 153, 135, 21);
		bills.add(textFieldPhoneNo);
		
		JLabel labelAddress = new JLabel("Address");
		labelAddress.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		labelAddress.setBounds(246, 182, 81, 21);
		bills.add(labelAddress);
		
		textFieldAddress = new JTextField();
		textFieldAddress.setColumns(10);
		textFieldAddress.setBounds(398, 178, 135, 31);
		bills.add(textFieldAddress);
		
		JLabel labelTotalBill = new JLabel("Total ");
		labelTotalBill.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		labelTotalBill.setBounds(246, 214, 120, 21);
		bills.add(labelTotalBill);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(398, 214, 400, 36);
		bills.add(textField);
		
		JLabel lblSubscriptionID = new JLabel("Subscription ID");
		lblSubscriptionID.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblSubscriptionID.setBounds(508, 41, 135, 14);
		bills.add(lblSubscriptionID);
		comboBoxSubsID.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		
		
		comboBoxSubsID.setModel(new DefaultComboBoxModel(new String[] {"Choose Subs ID"}));
		comboBoxSubsID.setToolTipText("");
		comboBoxSubsID.setBounds(653, 38, 159, 21);
		bills.add(comboBoxSubsID);
		
		JButton btnBill = new JButton("Show Bill");
		btnBill.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnBill.setBounds(596, 118, 114, 56);
		bills.add(btnBill);
		
		JLabel labelArea = new JLabel("Area");
		labelArea.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		labelArea.setBounds(246, 41, 61, 14);
		bills.add(labelArea);
		
		JComboBox comboBoxArea = new JComboBox();
		comboBoxArea.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		comboBoxArea.setModel(new DefaultComboBoxModel(new String[] {"Choose Area", "1", "2", "3", "4"}));
		comboBoxArea.setToolTipText("");
		comboBoxArea.setBounds(328, 38, 132, 21);
		bills.add(comboBoxArea);
		
		ActionListener billsButtonEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == btnBill) {
					int area = comboBoxArea.getSelectedIndex();
					File folder = new File("Invoice");
					File[] listOfFiles = folder.listFiles();
					comboBoxSubsID.setModel(new DefaultComboBoxModel(new String[] {"Choose Subs ID"}));
					    for (int i = 0; i < listOfFiles.length; i++) {
					    	System.out.println("hello");
					    	 System.out.println(listOfFiles[i].getName());
					      String [] billInput = listOfFiles[i].getName().split("_");
					      System.out.println(billInput[1]);
					      if(Integer.parseInt(billInput[1]) == area){
					    	  comboBoxSubsID.addItem(listOfFiles[i].getName());
					    	  logs.append(listOfFiles[i].getName() + "\n");
					      }
					    }
					
				}
			}
		};
		
		btnBill.addActionListener(billsButtonEvent);
		
		comboBoxSubsID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBoxSubsID.getSelectedIndex() > 0) {
					String fileName = (String) comboBoxSubsID.getSelectedItem();
					logs.setText("");
					try {
						String inputFileName = "Invoice/"+fileName;
						File inputFile = new File(inputFileName);
						Scanner in = new Scanner(inputFile);
						logs.append("\n\n Invoice No: "+fileName +"\n");
						int order = 0;
						while (in.hasNextLine()) {
							String value = in.nextLine();
							logs.append(value +"\n");
							if(order == 1){
								textFieldFirstName.setText(value);
							}
							if(order == 2){
								textFieldAddress.setText(value);
							}
							if(order == 3){
								textFieldPhoneNo.setText(value);
							}
							if(order == 4){
								textField.setText(value);
							}

							order++;
						}
						in.close();
					} catch (FileNotFoundException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

				}
			}
		});
/***************************************************************************************************************************************************/
		

		ActionListener radioButtonEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == cre) {
					creB.setVisible(true);
					modB.setVisible(false);
					archB.setVisible(false);
					customersFound.setVisible(false);
					b2P.setVisible(false);

					b3.removeAll();
					b4.removeAll();

					b3.add(firstName);
					b3.add(lastName);
					b3.add(dateOfBirth);
					b3.add(addressStreet);
					b3.add(phone);
					b3.add(email);
					b3.add(areaCode);

					b4.add(fNtxt);
					b4.add(lNtxt);
					b4.add(dOBtxt);
					b4.add(aStxt);
					b4.add(ptxt);
					b4.add(etxt);
					b4.add(aCtxt);

					b3.revalidate();
					b3.repaint();
					b4.revalidate();
					b4.repaint();

				} else {
					if (e.getSource() == sea) {
						creB.setVisible(false);
						modB.setVisible(true);
						archB.setVisible(false);
						customersFound.setVisible(true);
						b2P.setVisible(true);

					}
					if (e.getSource() == arch) {
						creB.setVisible(false);
						modB.setVisible(true);
						archB.setVisible(true);
						customersFound.setVisible(true);
						b2P.setVisible(true);
					}
					b3.remove(firstName);
					b3.remove(dateOfBirth);
					b3.remove(phone);
					b3.remove(email);
					b3.remove(areaCode);
					b3.add(custId);
					b3.add(subsId);

					b4.remove(fNtxt);
					b4.remove(dOBtxt);
					b4.remove(ptxt);
					b4.remove(etxt);
					b4.remove(aCtxt);
					b4.add(cIdtxt);
					b4.add(sIdtxt);

					b3.revalidate();
					b3.repaint();
					b4.revalidate();
					b4.repaint();
				}
			}
		};
		cre.addActionListener(radioButtonEvent);
		sea.addActionListener(radioButtonEvent);
		arch.addActionListener(radioButtonEvent);

		ActionListener LoginButtonsEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object target = e.getSource();
				if (target == manager) {
					JTextComponent pwd = new JPasswordField(10);
					int result = JOptionPane.showConfirmDialog(null, pwd, "Please input the Access password",
							JOptionPane.OK_CANCEL_OPTION);
					if (result == JOptionPane.OK_OPTION) {
						String pass = pwd.getText();
						if (pass.equals(password)) {
							cards.show(mid, "customer");
							botBut.setVisible(true);
						} else {
							JOptionPane.showMessageDialog(frame, "Sorry, Access Denied...", "Password ERROR",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				if (target == delivery) {
					cards.show(mid, "deliveries");
				}
				if (target == deliveryDetails) {
					cards.show(mid, "DeliveryDetails");
				}
			}
		};
		manager.addActionListener(LoginButtonsEvent);
		delivery.addActionListener(LoginButtonsEvent);

		ActionListener CustomerButtonsEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object target = e.getSource();
				if (target == creB) {
					String fN = fNtxt.getText();
					String lN = lNtxt.getText();
					String dOB = dOBtxt.getText();
					String aS = aStxt.getText();
					String p = ptxt.getText();
					String em = etxt.getText();
					String a = aCtxt.getText();

					// send to Kalos's method for check
					customer.Create_Customer(fN, lN, dOB, aS, p, em, a);
				}
				if (target == searchB) {
					String choice = "";
					choiceFlag = 0;
					searchFlag = 0;
					if (nm.isSelected()) {
						choice = lNtxt.getText();
						choiceFlag = 1;
						searchFlag = 1;
					}
					if (add.isSelected()) {
						choice = aStxt.getText();
						choiceFlag = 2;
						searchFlag = 1;
					}
					if (cusId.isSelected()) {
						choice = cIdtxt.getText();
						choiceFlag = 3;
						searchFlag = 1;
					}
					if (subId.isSelected()) {
						choice = sIdtxt.getText();
						choiceFlag = 4;
						searchFlag = 1;
					}
					// if(choice.equals("")){System.out.println("empty");}
					// System.out.println(choice);
					if (!choice.equals("")) {
						System.out.println(choice);
						// Send to Kalos object.method(choice, choiceFlag);
						customer.Search_Customer(choice, choiceFlag);
					} else {
						JOptionPane.showMessageDialog(frame, "You need to Enter the value first...",
								"Search Input Error", JOptionPane.ERROR_MESSAGE);
					}
				}
				if (target == modB) {
					String fN = fNtxt.getText();
					String lN = lNtxt.getText();
					int dOB = Integer.valueOf(dOBtxt.getText());
					String aS = aStxt.getText();
					int p = Integer.valueOf(ptxt.getText());
					String em = etxt.getText();
					int a = Integer.valueOf(aCtxt.getText());
					int c = Integer.valueOf(cIdtxt.getText());
					//int s = Integer.valueOf(sIdtxt.getText());

					// send to Kalos's method for check
					// object.method(fN, lN, dOB, aS, p, em, a, c, s);
					customer.Update_Customer(c, fN, lN, dOB, aS, em, p, a);
				}
				if (target == archB) {
					int Cid = Integer.valueOf(cIdtxt.getText());
					// send to Kalos's method for check
					// Alaa needs to delete the customer's subscription as
					// well...
					// object.method(id);
					customer.Achieve_Customer(Cid);
				}
			}
		};
		creB.addActionListener(CustomerButtonsEvent);
		modB.addActionListener(CustomerButtonsEvent);
		searchB.addActionListener(CustomerButtonsEvent);
		archB.addActionListener(CustomerButtonsEvent);

		ActionListener SubscriptionButtonsEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object target = e.getSource();
				if (target == btnSearch) {
					String cusid = txtCustomerID.getText();
					String subid = txtSubscriptionID.getText();
					subs.Search_Subscription(cusid, subid);
					// System.out.println(i);
					// if(i==0)
					// DB.setselection("1");
					// customer.setselection("1");
					// if(i==1)
					// DB.setselection("2");
					// if(i==2)
					// DB.setselection("3");
				}
				if (target == btnSubscribe) {
					String cid = txtCustomerID.getText();
					String sid = txtSubscriptionID.getText();
					int[] pubs = new int[13];
					for (int i = 0; i < newsP.size(); i++) {
						if (((AbstractButton) newsP.get(i)).isSelected()) {
							// System.out.println(1);
							pubs[i] = 1;
						} else {
							pubs[i] = 0;
							// System.out.println(0);
						}
						System.out.println(pubs[i]);
					}
					// call Kalos's method to insert SUBSCRIPTION
					subs.Create_Subscription(cid, sid, pubs);
				}
			}
		};
		btnSearch.addActionListener(SubscriptionButtonsEvent);

		mainPanel.add(top, BorderLayout.NORTH);
		mainPanel.add(mid, BorderLayout.CENTER);
		mainPanel.add(bot, BorderLayout.SOUTH);
		mainPanel.add(left, BorderLayout.WEST);
		mainPanel.add(right, BorderLayout.EAST);

		cp.add(mainPanel);
		frame.setSize(900, 750);
		frame.setVisible(true);
	}

	public void createCustomer(String custID, String subsID) {
		String msg = "";
		if (!custID.equals("fail")) {
			int customersID = Integer.parseInt(custID);
			int subscriptionID = Integer.parseInt(subsID);
			msg = "Account succesfuly created \n" + "Customer's ID:   " + customersID + "\n" + "Subscription ID: "
					+ subscriptionID + "\n" + "You can Create New Subscription";

			cards.show(mid, "subscriptions");
			txtCustomerID.setText(custID);
			txtSubscriptionID.setText(subsID);
		} else {
			msg = subsID;
		}
		JOptionPane.showMessageDialog(null, msg, "PROGRESS INFORMATION", JOptionPane.WARNING_MESSAGE);
	}

	public void UpdateCustomer(String id, String msg) {
		String newMsg = "The Account " + id + "\n" + msg;
		JOptionPane.showMessageDialog(null, newMsg, "PROGRESS INFORMATION", JOptionPane.WARNING_MESSAGE);
		// update
	}

	public void createSubscription(String msg) {
		JOptionPane.showMessageDialog(null, msg, "PROGRESS INFORMATION", JOptionPane.WARNING_MESSAGE);
	}

	public void searchCustomer(String cid, String fn, String ln, String yob, String add, String phn, String em,
			String area, String sid) {
		if (choiceFlag != 0 && !fn.equals(null)) {
			customersFound.setModel(
					new DefaultComboBoxModel<String>(new String[] { " List of Customers found in the Data Base " }));
			choiceFlag = 0;
		}
		if (!fn.equals(null)) {
			String newListEntry = "" + cid + " , " + fn + " , " + ln + " , " + yob + " , " + add + " , " + phn + " , "
					+ em + " , " + area + " , " + sid;
			customersFound.addItem(newListEntry);
			if (searchFlag == 1) {
				b3.removeAll();
				b4.removeAll();

				b3.add(custId);
				b3.add(firstName);
				b3.add(lastName);
				b3.add(dateOfBirth);
				b3.add(addressStreet);
				b3.add(phone);
				b3.add(email);
				b3.add(areaCode);
				b3.add(subsId);

				b4.add(cIdtxt);
				b4.add(fNtxt);
				b4.add(lNtxt);
				b4.add(dOBtxt);
				b4.add(aStxt);
				b4.add(ptxt);
				b4.add(etxt);
				b4.add(aCtxt);
				b4.add(sIdtxt);

				b3.revalidate();
				b3.repaint();
				b4.revalidate();
				b4.repaint();
				searchFlag = 0;
			}
		}
		if (fn.equals(null)) {
			JOptionPane.showMessageDialog(null, cid, "PROGRESS INFORMATION", JOptionPane.WARNING_MESSAGE);
			searchFlag = 0;
		}

	}

	public void DeliveryPersonDetails(int id, String name) {
		txtDelPName.setText(name);
		txtDelPID.setText(String.valueOf(id));
	}
	
	public void deliveryList(String line){
		deliveriesDetails.addItem(line);
	}

	public void searchSubscription(int[] pubs) {
		String msg = "Subscription successfully Displayed";
		if (pubs != null) {
			for (int i = 0; i < pubs.length; i++) {
				if (pubs[i] == 1) {
					((AbstractButton) newsP.get(i)).setSelected(true);
				}
			}
		} else {
			msg = "The ID you have entered does not exist";
		}
		JOptionPane.showMessageDialog(null, msg, "SEARCH RESULT INFORMATION", JOptionPane.WARNING_MESSAGE);
	}

	public void addNewSubscription(String subId, String msg) {
		String newMsg = "";
		if (subId == null) {
			newMsg = msg;
		} else {
			newMsg = "The Subscription ID:  " + subId + "\n" + msg;
		}
		JOptionPane.showMessageDialog(null, newMsg, "CREATE Subs. RESULT INFORMATION", JOptionPane.WARNING_MESSAGE);
	}
	public void PrintDelivery(String line) {
		logs.append(line+"\n");
	}
	public void getBills() {

	}	
		
		
	public void connectToCustomer(Customer customer) {
		this.customer = customer;
	}

	public void connectToDelivery(Delivery deliv) {
		this.deliv = deliv;
	}

	public void connectToDB(Database DB) {
		this.DB = DB;
	}

	public void connectToSubscription(SubScription subs) {
		this.subs = subs;
	}
}
