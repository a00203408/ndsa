import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;

public class Database {
	/** Database Connection */
	private Connection con;

	/** Used for executing database statements */
	private Statement stmt;

	/** Holds result of stmt, moves to next row in database */
	private ResultSet rs;

	/** Holds the number of rows in database value */
	int count;

	/** Holds the current number of the row in the database we are on */
	int current;

	private GUI gui;

	private String cus_firstName;
	private String cus_lastName;
	private String cus_address;
	private String cus_email;
	private String cus_phone;
	private String cus_id;
	private String cus_Year;
	private String cus_area;

	private String sub;
	private String cus;
	private int[] publications;
	private String hEnd;
	private String sub_Id;
	private int subs_Id;

	private String area = null;
	private int dpid;
	private String dpName;

	public Database() {
		con = null; /**
					 * @param con
					 *            the connection parameter to connect to MS Access database.
					 */
		stmt = null; /**
						 * @param stmt
						 *            the statement parameter to execute database statements.
						 */
		rs = null; /**
					 * @param rs
					 *            the result parameter to hold the result of statement and move
					 *            rows.
					 */
		count = 0; /**
					 * @param count
					 *            the int value to count number of rows in database.
					 */
		current = 0; /**
						 * @param current
						 *            the int value to keep track of which row in database we are on.
						 */

		dbConn();// method to connect to database using odbc-jdbc
		initDB();// method to initialise gui with database info

	}

	public void connectToGui(GUI gui) {
		this.gui = gui;
	}

	private void initDB() {
		// TODO Auto-generated method stub

	}

	public void dbConn() {
		try {
			// driver to use with named database which is stored alongside program in C
			// Drive
			String url = "jdbc:ucanaccess://DB/backEnd.accdb";
			// String url = "jdbc:ucanaccess://c:/Agile/cc.accdb";

			// connection represents a session with a specific database
			con = DriverManager.getConnection(url);

			// stmt used for executing sql statements and obtaining results
			stmt = con.createStatement();
			System.out.println("Connected");
			rs = stmt.executeQuery("SELECT * FROM Customer");

			while (rs.next()) // count number of rows in table
			{
				count++;

			}
			System.out.println(count);

			rs.close();
		} catch (Exception e) {
			System.out.println("Unable to connect to database");
		}
	}

	public void addNewCustomer(String firstName, String lastName, String YearOfBirth, String address, String email,
			String phone, String Area) {
		boolean check = false;
		String new_id = "";
		String new_subid = "";

		try {
			this.cus_Year = YearOfBirth;
			this.cus_firstName = firstName;
			this.cus_lastName = lastName;
			this.cus_address = address;
			this.cus_email = email;
			this.cus_phone = phone;
			this.cus_area = Area;

			Calendar now = Calendar.getInstance();
			String dates = "" + (now.get(Calendar.MONTH) + 1) + "_" + now.get(Calendar.DAY_OF_MONTH) + "_"
					+ now.get(Calendar.YEAR);
			System.out.println(dates);
			String checkEmail = "SELECT Email FROM Customer";
			rs = stmt.executeQuery(checkEmail);
			while (rs.next()) {
				if (cus_email.equals(rs.getString("Email"))) {

					check = true;
					break;
				}

			}

			if (check == true) {

				new_id = "fail";
				new_subid = "Email already existed";
			} else {
				String customer = "INSERT INTO Customer(FirstName, LastName, YearOfBirth, Address, Email, PhoneNumber, Area, Dates)VALUES('"
						+ cus_firstName + "', '" + cus_lastName + "', '" + cus_Year + "', '" + cus_address + "', '"
						+ cus_email + "', '" + cus_phone + "', '" + cus_area + "', '" + dates + "')";
				stmt.execute(customer);

				// count++;

				String cusIdt = "Select c.Cus_Id, s.Sub_Id from Customer c, Subscription s where c.Cus_Id = s.Cus_Id order by c.Cus_Id desc limit 1";
				rs = stmt.executeQuery(cusIdt);
				while (rs.next()) // count number of rows in table
				{
					count++;
					new_id = rs.getString(1);
					new_subid = rs.getString(2);
				}
				rs = stmt.executeQuery("Select MAX(Cus_Id) From Customer");
				int news = 0;
				if (rs.next()) {
					news = rs.getInt(1);
				}
				rs = stmt.executeQuery("Select MAX(Sub_Id) From Subscription");
				int newss = 0;
				if (rs.next()) {
					newss = rs.getInt(1);
				}
				newss++;
				new_id = Integer.toString(news);
				new_subid = Integer.toString(newss);
			}

			gui.createCustomer(new_id, new_subid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new_id = "fail";
			new_subid = e.toString();
		} finally {
			// gui.createCustomer(new_id, new_subid);
			System.out.println(new_id + new_subid);
		}
	}

	public void updateCustomer(String id, String firstName, String lastName, String YearOfBirth, String address,
			String email, String phone, String Area)
	// boolean cus=false;
	{
		boolean check = false;
		String message = "";
		try {
			this.cus_area = Area;
			this.cus_id = id;
			this.cus_firstName = firstName;
			this.cus_lastName = lastName;
			this.cus_address = address;
			this.cus_email = email;
			this.cus_phone = phone;
			this.cus_Year = YearOfBirth;

			String checkId = "SELECT Cus_Id FROM Customer";
			rs = stmt.executeQuery(checkId);
			while (rs.next()) {
				if (cus_id.equals(rs.getString("Cus_Id"))) {
					check = true;
					break;
				}
			}

			if (check != true) {
				message = "Id doesn't exist";
			} else {
				String updateTemp = "UPDATE Customer SET " + "FirstName = '" + cus_firstName + "', LastName = '"
						+ cus_lastName + "', Address = '" + cus_address + "', Email ='" + cus_email
						+ "', PhoneNumber = '" + cus_phone + "' where Cus_Id = " + cus_id;
				stmt.execute(updateTemp);
				message = "Updates were carried on";

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = e.toString();

		} finally {
			gui.UpdateCustomer(cus_id, message);
			System.out.println(cus_id + message);
		}
	}

	public void deleteCustomer(String id) {
		// boolean cus=false;
		String message = "";
		boolean check = false;
		try {
			this.cus_id = id;

			String checkId = "SELECT Cus_Id FROM Customer";
			rs = stmt.executeQuery(checkId);
			while (rs.next()) {
				if (cus_id.equals(rs.getString("Cus_Id"))) {
					check = true;
					break;
				}
			}

			if (check != true) {
				message = "Id doesn't exist";
			} else {

				String deletesub = "DELETE From Subscription where Subscription.Cus_Id= " + cus_id;
				stmt.execute(deletesub);
				count--;

				String deleteTemp = "DELETE From Customer where Cus_Id = " + cus_id;
				stmt.execute(deleteTemp);
				count--;

				// System.out.println();
				message = "Customer has been deleted";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = e.toString();
		} finally {
			// gui.DeleteCustomer(cus_id,message);
			System.out.println(cus_id + message);
		}
	}

	public void addNewSubscription(String cusId, int[] pub) {
		String new_subid = "";
		String message = "";
		try {
			// this.sub = subId;
			this.cus = cusId;
			this.publications = pub;

			String subscription = "INSERT INTO Subscription(Cus_Id, Newspaper1, Newspaper2, Newspaper3, Newspaper4, Newspaper5, Magazine1,  Magazine2, Magazine3, Magazine4, Magazine5, MagazineM1, MagazineM2, MagazineM3)VALUES('"
					+ cus + "', '" + publications[0] + "', '" + publications[1] + "', '" + publications[2] + "', '"
					+ publications[3] + "', '" + publications[4] + "', '" + publications[5] + "', '" + publications[6]
					+ "', '" + publications[7] + "', '" + publications[8] + "', '" + publications[9] + "', '"
					+ publications[10] + "', '" + publications[11] + "', '" + publications[12] + "')";
			stmt.execute(subscription);

			String subIdt = "Select Sub_Id from Subscription order by Sub_Id desc limit 1";
			rs = stmt.executeQuery(subIdt);
			while (rs.next()) // count number of rows in table
			{
				count++;
				new_subid = rs.getString(1);
			}

			message = "insert is successful";
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message = "alteration is not successful, please check the customer id. it is an integration error";
		} finally {
			gui.addNewSubscription(new_subid, message);
			System.out.println(new_subid + message);

		}
	}

	public void updateSubscription(String subId, String cusId, int[] pub) {
		String message = "";
		try {
			this.sub = subId;
			this.cus = cusId;
			this.publications = pub;
			boolean found = false;

			String checkId = "SELECT Sub_Id FROM Subscription";
			rs = stmt.executeQuery(checkId);
			while (rs.next()) {
				if (sub.equals(rs.getString("Sub_Id"))) {
					found = true;
				}
			}
			if (found != true) {
				message = "Subscription id doesn't exist";
				// gui.UpdateSubscription(sub_id, message);
			} else {
				String updateTemp = "UPDATE Subscription SET " + "Sub_Id = '" + sub + "', Cus_Id = '" + cus
						+ "', Newspaper1 = '" + publications[0] + "',Newspaper2 = '" + publications[1]
						+ "', Newspaper3 = '" + publications[2] + "', Newspaper4 = '" + publications[3]
						+ "', Newspaper5 = '" + publications[4] + "', Magazine1 = '" + publications[5]
						+ "', Magazine2 = '" + publications[6] + "',Magazine3 = '" + publications[7]
						+ "', Magazine4 = '" + publications[8] + "', Magazine5 = '" + publications[9]
						+ "',MagazineM1 = '" + publications[10] + "', MagazineM2 = '" + publications[11]
						+ "', MagazineM3 = '" + publications[12] + "' where Sub_Id = " + sub_Id;

				stmt.executeUpdate(updateTemp);
				message = "Subscription has been altered";

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// message = e.toString();
			message = "insert is not successful, please check the customer id it is an integration error";

		} finally {
			// gui.UpdateSubscription(sub,message);
			System.out.println(sub + message);
		}
	}

	public void searchCustomer(String nameToFind) {
		int foundCus = 0;
		boolean found = false;

		try {

			rs = stmt
					.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and c.LastName = '"
							+ nameToFind + "'");

			while (rs.next() && found == false) {
				foundCus++;
				String CusId = rs.getString("Cus_Id");
				String CusName = rs.getString("FirstName");
				String Surname = rs.getString("LastName");
				String Age = rs.getString("YearOfBirth");
				String CusAddress = rs.getString("Address");
				String CusEmail = rs.getString("Email");
				String CusPhoneNo = rs.getString("PhoneNumber");
				String Cusarea = rs.getString("Area");
				String CusSubId = rs.getString("Sub_Id");

				if (Surname.equals(nameToFind)) {
					gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea,
							CusSubId);

					System.out.println(CusId + " " + CusName + " " + Surname + " " + Age + " " + CusAddress + " "
							+ CusPhoneNo + " " + CusEmail + " " + Cusarea + " " + CusSubId);
					found = true;
				}
			}
			rs.close();
		} catch (Exception e) {
			gui.searchCustomer(null, "Error in finding customer in database", null, null, null, null, null, null, null);
			System.out.println("Error in finding customer in database");
		}
	}

	public void searchCustomerId(String idToFind) {
		int foundCus = 0;
		boolean found = false;

		try {

			rs = stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and c.Cus_Id = '"
					+ idToFind + "'");

			while (rs.next() && found == false) {
				foundCus++;
				String CusId = rs.getString("Cus_Id");
				String CusName = rs.getString("FirstName");
				String Surname = rs.getString("LastName");
				String Age = rs.getString("YearOfBirth");
				String CusAddress = rs.getString("Address");
				String CusEmail = rs.getString("Email");
				String CusPhoneNo = rs.getString("PhoneNumber");
				String Cusarea = rs.getString("Area");
				String CusSubId = rs.getString("Sub_Id");

				if (CusId.equals(idToFind)) {
					System.out.println(CusId + " " + CusName + " " + Surname + " " + Age + " " + CusAddress + " "
							+ CusPhoneNo + " " + CusEmail + " " + Cusarea + " " + CusSubId);

					gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea,
							CusSubId);
					found = true;
				}

			}
			rs.close();
		} catch (Exception e) {
			gui.searchCustomer(null, "Error in finding customer in database", null, null, null, null, null, null, null);
			System.out.println("Error in finding customer in database");
		}
	}

	public void searchCustomerSubId(String subIdToFind) {
		int foundCus = 0;
		boolean found = false;

		try {

			rs = stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and s.Sub_Id = '"
					+ subIdToFind + "'");

			while (rs.next() && found == false) {
				foundCus++;
				String CusId = rs.getString("Cus_Id");
				String CusName = rs.getString("FirstName");
				String Surname = rs.getString("LastName");
				String Age = rs.getString("YearOfBirth");
				String CusAddress = rs.getString("Address");
				String CusEmail = rs.getString("Email");
				String CusPhoneNo = rs.getString("PhoneNumber");
				String Cusarea = rs.getString("Area");
				String CusSubId = rs.getString("Sub_Id");

				if (CusSubId.equals(subIdToFind)) {
					System.out.println(CusId + " " + CusName + " " + Surname + " " + Age + " " + CusAddress + " "
							+ CusPhoneNo + " " + CusEmail + " " + Cusarea + " " + CusSubId);

					gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea,
							CusSubId);
					found = true;
				}

			}
			rs.close();
		} catch (Exception e) {
			gui.searchCustomer(null, "Error in finding customer in database", null, null, null, null, null, null, null);
			System.out.println("Error in finding customer in database");
		}
	}

	public void searchCustomerAddress(String addressToFind) {
		int foundCus = 0;
		boolean found = false;

		try {

			rs = stmt.executeQuery("SELECT * FROM Customer c,Subscription s Where c.Cus_Id = s.Cus_Id and c.Address = '"
					+ addressToFind + "'");

			while (rs.next() && found == false) {
				foundCus++;
				String CusId = rs.getString("Cus_Id");
				String CusName = rs.getString("FirstName");
				String Surname = rs.getString("LastName");
				String Age = rs.getString("YearOfBirth");
				String CusAddress = rs.getString("Address");
				String CusEmail = rs.getString("Email");
				String CusPhoneNo = rs.getString("PhoneNumber");
				String Cusarea = rs.getString("Area");
				String CusSubId = rs.getString("Sub_Id");

				if (CusAddress.equals(addressToFind)) {
					System.out.println(CusId + " " + CusName + " " + Surname + " " + Age + " " + CusAddress + " "
							+ CusPhoneNo + " " + CusEmail + " " + Cusarea + " " + CusSubId);
					gui.searchCustomer(CusId, CusName, Surname, Age, CusAddress, CusPhoneNo, CusEmail, Cusarea,
							CusSubId);
				}

			}
			found = true;
			rs.close();
		} catch (Exception e) {
			gui.searchCustomer(null, "Error in finding customer in database", null, null, null, null, null, null, null);
			System.out.println("Error in finding customer in database");
		}
	}

	public void searchSubscription(String CusIdToFind, String SubIdToFind) {
		int foundSub = 0;
		boolean found = false;
		int[] subs = new int[13];

		try {

			rs = stmt.executeQuery("SELECT * FROM Subscription Where Sub_Id = '" + SubIdToFind + "' and Cus_Id = '"
					+ CusIdToFind + "'");
			// System.out.println("SELECT * FROM Subscription Where Sub_Id =
			// '"+SubIdToFind+"' and Cus_Id = '"+CusIdToFind+"'");
			while (rs.next() && found == false) {

				String CusId = rs.getString("Cus_Id");
				String SubId = rs.getString("Sub_Id");

				subs[0] = rs.getInt("Newspaper1");
				subs[1] = rs.getInt("Newspaper2");
				subs[2] = rs.getInt("Newspaper3");
				subs[3] = rs.getInt("Newspaper4");
				subs[4] = rs.getInt("Newspaper5");

				subs[5] = rs.getInt("Magazine1");
				subs[6] = rs.getInt("Magazine2");
				subs[7] = rs.getInt("Magazine3");
				subs[8] = rs.getInt("Magazine4");
				subs[9] = rs.getInt("Magazine5");

				subs[10] = rs.getInt("MagazineM1");
				subs[11] = rs.getInt("MagazineM2");
				subs[12] = rs.getInt("MagazineM3");
				System.out.println("database");
				if (SubId.equals(SubIdToFind) && CusId.equals(CusIdToFind)) {
					for (int i = 0; i < subs.length; i++) {
						System.out.println(subs[i]);

					}
					gui.searchSubscription(subs);
					found = true;
				}

			}
			rs.close();
		} catch (Exception e) {
			gui.searchSubscription(null);
			System.out.println(e);
		}
	}

	public void DeliveryDetails(String areas) {
		String name = "";
		int id = 0;
		String areaCode = "";

		try {
			this.area = areas;

			String deliveries = "Select * from DeliveryPerson where Area ='" + areas + "'";
			rs = stmt.executeQuery(deliveries);

			while (rs.next()) // count number of rows in table
			{
				id = rs.getInt(1);
				name = rs.getString(2);
			}
			area = areaCode;
			// System.out.println("delivery person is accessed");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			gui.DeliveryPersonDetails(id, name);
			System.out.println(id + name);
		}
	}

	/*
	 * public void DeliveryHistory(String area, String dates) { int subId =0; String
	 * Cus_name = ""; String Address = ""; String phone = ""; int [] subs=null; area
	 * = null; try { String history =
	 * "Select s.Sub_Id, c.FirstName, c.LastName, c.Address, c.PhoneNumber,  c.Area, Newspaper1, Newpaper2, Newspaper3, Newpaper4, Newpaper5, Magazine1,  Magazine2, Magazine3, Magazine4, Magazine5, MagazineM1, MagazineM2, MagazineM3 from Customer c, Subscription s where c.Area ='"
	 * +area+"'"; rs = stmt.executeQuery(history);
	 * 
	 * 
	 * while(rs.next()) // count number of rows in table { subId=rs.getInt(1);
	 * Cus_name=rs.getString(2)+(3); Address=rs.getString(4); phone=rs.getString(5);
	 * Address=rs.getString(6);
	 * 
	 * for(int i=0; i<13;i++) { subs[i]=rs.getInt(i+7); }
	 * 
	 * //subs=rs.getInt(3); } System.out.println("retrive is completed");
	 * writeToFile(rs, dates,area); } catch (SQLException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }finally {
	 * //gui.DeliveryDetails(Cus_name,Address,phone,Address, subs); } }
	 */

	public void DeliveryHistory(String area) throws Exception {
		String datess = null;
		int id = 0;
		String name = "";
		int subId = 0;
		String Cus_name = "";
		String Cus_fname = "";
		String Address = "";
		String phone = "";
		int[] subs = new int[13];
		String x = null;
		String y = null;
		String m = null;
		String dp = null;
		String message = null;
		try {

			boolean found = false;

			String checkArea = "SELECT Area FROM Customer";
			rs = stmt.executeQuery(checkArea);
			while (rs.next()) {
				if (area.equals(rs.getString("Area"))) {
					found = true;
				}
			}
			if (found != true) {
				message = "Area doesn't exist";

			} else {
				String deliveries = "Select * from DeliveryPerson where Area ='" + area + "'";
				rs = stmt.executeQuery(deliveries);

				while (rs.next()) // count number of rows in table
				{
					id = rs.getInt(1);
					name = rs.getString(2);
				}
				dp = ("Delivery person :  " + id + "	" + name);
				Calendar now = Calendar.getInstance();
				datess = "" + (now.get(Calendar.MONTH) + 1) + "_" + now.get(Calendar.DAY_OF_MONTH) + "_"
						+ now.get(Calendar.YEAR) + "_" + area;
				System.out.println(datess);
				String datess2 = "" + (now.get(Calendar.MONTH) + 1) + "_" + now.get(Calendar.DAY_OF_MONTH) + "_"
						+ now.get(Calendar.YEAR) + "_" + area;

				String history = "Select s.Sub_Id, c.FirstName, c.LastName, c.Address, c.PhoneNumber,  c.Area, Newspaper1, Newspaper2, Newspaper3, Newspaper4, Newspaper5, Magazine1,  Magazine2, Magazine3, Magazine4, Magazine5, MagazineM1, MagazineM2, MagazineM3 from Customer c, Subscription s where c.Cus_Id = s.Cus_Id and c.Area ='"
						+ area + "'";
				rs = stmt.executeQuery(history);

				FileWriter fileWriter = new FileWriter("DB/Delivery_" + datess + ".txt");
				fileWriter.write(dp);
				while (rs.next()) // count number of rows in table
				{
					subId = rs.getInt(1);
					Cus_fname = rs.getString(2);
					Cus_name = rs.getString(3);
					Address = rs.getString(4);
					phone = rs.getString(5);
					area = rs.getString(6);
					System.out.println("Sub Id\n" + "Cus_name\n" + "Address\n" + "phone\n" + "area\n");
					x = ("Sub Id\t" + "Cus_name\t" + "Address\t" + "phone\t" + " area\t");
					System.out.println(
							subId + "\t\t" + Cus_fname + " " + Cus_name + "\t" + Address + "t" + phone + "\t" + area);
					y = (subId + "\t" + Cus_fname + " " + Cus_name + "\t" + Address + "\t" + phone + "\t" + area);

					for (int i = 0; i < 13; i++) {
						subs[i] = rs.getInt(i + 7);
					}

					String y1 = "" + subId + " @ " + Cus_name + " @ " + Address + " @ " + phone + " @ "
							+ Arrays.toString(subs) + " @ " + datess2;
					gui.deliveryList(y1);
					System.out.println("meeeeeeee" + y1);

					System.out.println(Arrays.toString(subs));
					// m= (Arrays.toString(subs));
					String all = (y + m);
					// gui.deliveryList(all);

					try {
						for (int i = 0; i < 1; i++) {
							fileWriter.write("\n" + x + "\n" + y + "\n" + m + "[]");
							fileWriter.flush();
						}
					} catch (FileNotFoundException x1) {
						x1.getMessage();
					}

				}
				message = ("retrive is completed");
				fileWriter.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		} finally {
			System.out.println(message);
		}
	}

	public void Undelivery(int sub_id, String dates, String areass, String publication, String name, String address,
			String phone) throws Exception {
		String dp = null;
		int id = 0;
		String names = null;
		String subId = null;
		String area2 = null;
		String pubs = null;
		String addresss = null;
		String phones = null;
		String Cus_name = null;
		String x = null, y = null;
		String message = null;
		String dated = null;
		try {
			this.subs_Id = sub_id;

			String undel = "INSERT INTO Undelivery(Sub_Id, Dates, Area, Publication, CustomerName, Address, PhoneNumber)VALUES('"
					+ sub_id + "','" + dates + "','" + areass + "','" + publication + "','" + name + "','" + address
					+ "','" + phone + "') ";
			stmt.execute(undel);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// gui.Undel(sub_id, message);
			System.out.println(message);
		}
	}

	public void unFile(String areass) {
		String message = null;
		String dp = null;
		int id = 0;
		String names = null;
		String x = null, y = null;
		try {

			Calendar now = Calendar.getInstance();
			String dated = "" + (now.get(Calendar.MONTH) + 1) + "_" + now.get(Calendar.DAY_OF_MONTH) + "_"
					+ now.get(Calendar.YEAR) + "_" + area;
			System.out.println(dated);

			String deliveries = "Select * from DeliveryPerson where Area ='" + areass + "'";
			rs = stmt.executeQuery(deliveries);

			while (rs.next()) // count number of rows in table
			{
				id = rs.getInt(1);
				names = rs.getString(2);
			}
			dp = ("Delivery person :  " + id + "	" + names);

			String und = "Select * from Undelivery where Area = '" + areass + "'";
			rs = stmt.executeQuery(und);
			FileWriter fileWriter = new FileWriter("DB/Undelivered_" + dated + ".txt");
			fileWriter.write(dp);
			while (rs.next()) {
				String subId = rs.getString("Sub_Id");
				String area2 = rs.getString("Area");
				String datess = rs.getString("Date");
				String Cus_name = rs.getString("CustomerName");
				String pubs = rs.getString("Publication");
				String addresss = rs.getString("Address");
				String phones = rs.getString("PhoneNumber");
				System.out.println("Sub Id\n" + "Area\n" + "Publication\n" + "Address\n" + "PhoneNumber\n");
				x = ("subId\t" + "Cus_name\t" + "addresss\t" + "Publication\t" + "phones\t" + " area2\t");
				System.out.println(subId + "\t" + Cus_name + " " + addresss + "\t" + datess + "\t" + pubs + "\t"
						+ phones + "\t" + area2);
				y = (subId + "\t" + Cus_name + " " + addresss + "\t" + pubs + "\t" + datess + "\t" + phones + "\t"
						+ area2);

				try {
					for (int i = 0; i < 1; i++) {
						fileWriter.write("\n" + x + "\n" + y + "\t" + "[]");

						fileWriter.flush();
					}
				} catch (FileNotFoundException x1) {
					x1.getMessage();
				}

			}
			message = ("retrive is completed");
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			message = ("error retrieving the customer");

		}
	}

	public void PrintDelivery(String area) throws Exception {
		String datess = null;
		int id = 0;
		String name = "";
		int subId = 0;
		String Cus_name = "";
		String Cus_fname = "";
		String Address = "";
		String phone = "";
		int[] subs = new int[13];
		String x = null;
		String y = null;
		String m = null;
		String dp = null;
		String message = null;
		try {

			boolean found = false;

			String checkArea = "SELECT Area FROM Customer";
			rs = stmt.executeQuery(checkArea);
			while (rs.next()) {
				if (area.equals(rs.getString("Area"))) {
					found = true;
				}
			}
			if (found != true) {
				message = "Area doesn't exist";

			} else {
				String deliveries = "Select * from DeliveryPerson where Area ='" + area + "'";
				rs = stmt.executeQuery(deliveries);

				while (rs.next()) // count number of rows in table
				{
					id = rs.getInt(1);
					name = rs.getString(2);
				}
				dp = ("Delivery person :  " + id + "	" + name);
				Calendar now = Calendar.getInstance();
				datess = "" + (now.get(Calendar.MONTH) + 1) + "_" + now.get(Calendar.DAY_OF_MONTH) + "_"
						+ now.get(Calendar.YEAR) + "_" + area;
				System.out.println(datess);
				String datess2 = "" + (now.get(Calendar.MONTH) + 1) + "_" + now.get(Calendar.DAY_OF_MONTH) + "_"
						+ now.get(Calendar.YEAR) + "_" + area;

				String history = "Select s.Sub_Id, c.FirstName, c.LastName, c.Address, c.PhoneNumber,  c.Area, Newspaper1, Newspaper2, Newspaper3, Newspaper4, Newspaper5, Magazine1,  Magazine2, Magazine3, Magazine4, Magazine5, MagazineM1, MagazineM2, MagazineM3 from Customer c, Subscription s where c.Cus_Id = s.Cus_Id and c.Area ='"
						+ area + "'";
				rs = stmt.executeQuery(history);

				FileWriter fileWriter = new FileWriter("DB/Delivery_" + datess + ".txt");
				fileWriter.write(dp);
				while (rs.next()) // count number of rows in table
				{
					subId = rs.getInt(1);
					Cus_fname = rs.getString(2);
					Cus_name = rs.getString(3);
					Address = rs.getString(4);
					phone = rs.getString(5);
					area = rs.getString(6);
					System.out.println("Sub Id\n" + "Cus_name\n" + "Address\n" + "phone\n" + "area\n");
					x = ("Sub Id\t" + "Cus_name\t" + "Address\t" + "phone\t" + " area\t");
					System.out.println(
							subId + "\t\t" + Cus_fname + " " + Cus_name + "\t" + Address + "t" + phone + "\t" + area);
					y = (subId + "\t" + Cus_fname + " " + Cus_name + "\t" + Address + "\t" + phone + "\t" + area);

					for (int i = 0; i < 13; i++) {
						subs[i] = rs.getInt(i + 7);
					}

					String y1 = "" + subId + " @ " + Cus_name + " @ " + Address + " @ " + phone + " @ "
							+ Arrays.toString(subs) + " @ " + datess2;
					gui.PrintDelivery(y1);
					System.out.println("meeeeeeee" + y1);

					System.out.println(Arrays.toString(subs));
					// m= (Arrays.toString(subs));
					String all = (y + m);
					// gui.deliveryList(all);

					try {
						for (int i = 0; i < 1; i++) {
							fileWriter.write("\n" + x + "\n" + y + "\n" + m + "[]");
							fileWriter.flush();
						}
					} catch (FileNotFoundException x1) {
						x1.getMessage();
					}

				}
				message = ("retrive is completed");
				fileWriter.close();
				//if (2 == now.get(Calendar.DAY_OF_MONTH)) {
					this.bill(area);
				//}
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		} finally {
			System.out.println(message);
		}
	}

	
	public void bill(String area) throws Exception {
		ArrayList<String[]> area_Customer=new ArrayList<String[]>();
		rs = stmt.executeQuery("select Cus_id,Dates,days from Customer where Area='"+area+"'");
		while(rs.next()){
			String[] str=new String[4];
			for(int i=1;i<4;i++) {
				str[i-1]=rs.getString(i);
			}
			area_Customer.add(str);
		}
		
		ArrayList<String[]> Customer_sub=new ArrayList<String[]>();
		Iterator it=area_Customer.iterator();
		while(it.hasNext()) {
			String[] cus=(String [])it.next();
			rs = stmt.executeQuery("select Cus_Id, Newspaper1, Newspaper2, Newspaper3, Newspaper4, Newspaper5, Magazine1,  Magazine2, Magazine3, Magazine4, Magazine5, MagazineM1, MagazineM2, MagazineM3 from Subscription where Cus_id='"+cus[0]+"'");
			while(rs.next()){
				String[] sub=new String[14];
				for(int i=3;i<17;i++) {
					sub[i-3]=rs.getString(i-2);
				}
				Customer_sub.add(sub);
			}
		}
		
		rs=stmt.executeQuery("select price from Publication");
		double[] price=new double[13];
		double[] total=new double[13];
		int p=0;
		while(rs.next()){
			String pri=rs.getString(1);
			price[p]=Double.parseDouble(pri);
			p++;
		}
		Iterator it2=Customer_sub.iterator();
		while(it2.hasNext()){
			String[] sub=(String[])it2.next();
			System.out.print("Customer id: "+sub[0]+" ");
			for(int i=1;i<sub.length;i++){
				//System.out.print(sub[i]+"*"+price[i-1]+" ; ");
				total[i-1]=(price[i-1])*Integer.parseInt(sub[i]);
			}
			int sum=0;
			for(int i=0;i<total.length;i++) {
				sum+=total[i];
			}System.out.print("Total :"+sum+"�");
			
			/*String id = null;
			String name = null;
			String address = null;
			String phone = null;
			String xxxx = ("Select * from Customer where Area =" + area);
			rs = stmt.executeQuery(xxxx);
			while (rs.next()) {
				id = rs.getString("Cus_Id"); 
				name = rs.getString("FirstName") + rs.getString("LastName");
				address = rs.getString("Address");
				phone = rs.getString("PhoneNumber");
			}*/
			
			
			Calendar now = Calendar.getInstance();
			String dates = "" + (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DAY_OF_MONTH) + "-"
					+ now.get(Calendar.YEAR);
		
			String year = "" + (now.get(Calendar.YEAR));
			String month = "" + (now.get(Calendar.MONTH));
			String billDate = ("2-" + month + "-" + year);
			int deliveryFee = 5;
			String dp = null;
			
			
			String cus1,fname1,lname1,add1,pho1,subs1;
			rs = stmt.executeQuery("Select c.Cus_Id, s.Sub_Id, c.FirstName, c.LastName, c.Address, c.PhoneNumber from Customer c, Subscription s where c.Cus_Id = s.Cus_Id and Area = "+area);
			while (rs.next()) {
				cus1 = rs.getNString("Cus_Id");
				fname1 = rs.getString("FirstName");
				lname1 = rs.getString("LastName");
				add1 = rs.getString("Address");
				pho1 = rs.getString("PhoneNumber");
				subs1 = rs.getString("Sub_Id");
				String dir = ("Invoice/");
				String filename = "Invoice-" + year + month +"_"+ area +"_"+cus1+ ".txt";
				FileWriter fileWriter = new FileWriter(dir+filename);
			

				System.out.println("name\n" + "address\n" + "phone\n" + "area\n" + "Subscription Total\n+ "+"Delivery Fee\n" +"Amount owned for this month\n"+billDate);
				//String x = ("name" + "\t" + "address" + "\t" + "phone" + "\t" + "subscribtions" + "\t" + "area" + "\t" + "Subscription Total"+ "\t" +"Delivery Fee" + "\t" +"Amount owned for this month "+billDate);
				//System.out.println(name + "\t" + address + " " + phone + "\t" + area + "\t" +" � "+sum+ "\t" +" � " +deliveryFee+ "\t" +" � "+sum+deliveryFee);
				int tot = sum+deliveryFee;
				String y = (fname1 + " " +lname1+ "\n" + add1 + "\n" + pho1 + "\n"  +" Subscribtions fee: � "+sum+ " \t " +" Delivery Fee: � "+deliveryFee+ "\t " +"total: � "+tot);
		
				String y1 = "" + fname1 + " @ " +lname1 + " @ " + add1 + " @ " + pho1 + " @ " + area+ " @ " +" Subscribtions fee: � "+sum+ " @ " +" M Delivery Fee: � "+deliveryFee+ " @ " +"total: � "+tot;
				//gui.bill(y1, subs1);
				System.out.println("meeeeeeee" + y1);
				gui.PrintDelivery(filename);
				
				try {
					for (int i = 0; i < 1; i++) {
						fileWriter.write("\n"+ y + "\n" );
						fileWriter.flush();
					}
				} catch (FileNotFoundException x1) {
					x1.getMessage();
				}
				
				fileWriter.close();
			
			

			// gui.invoice(subs1,fname1, lname1, add1, pho1,  y1);
				
			}
			

			
		}
	}
}
