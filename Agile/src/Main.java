
public class Main {

	public static void main(String[] args) {
		GUI gui = new GUI();		
		Database system = new Database();
		Customer customer = new Customer();
		Delivery deliv = new Delivery();
		
		system.connectToGui(gui);
		gui.connectToDB(system);
		customer.connectToGUI(gui);
		gui.connectToCustomer(customer);
		customer.connectToDB(system); 
		gui.connectToDelivery(deliv);
		deliv.connectToDB(system);
	}

}
